/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.graph.test;

import com.graphs.Graph;
import com.graphs.Node;
import com.graphs.test.Country;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Mikolaj
 */
public class AlgorithmsTest {
    private Graph<Country> graph;
    
    @Before
    public void createGraph() {
        graph = new Graph<>();
        Node<Country> pl = new Node<>(new Country("Polska", "Warszawa", 1231321, 65465465));
        Node<Country> de = new Node<>(new Country("Niemcy", "Berlin", 4354545, 3453543));
        Node<Country> ru = new Node<>(new Country("Rosja", "Moskwa", 1231065461, 3453453));
        Node<Country> fr = new Node<>(new Country("Francja", "Paryz", 1120000, 34534512));
        Node<Country> cz = new Node<>(new Country("Czechy", "Praga", 12121212, 12121214));
        graph.addNode(pl);
        graph.addNode(de);
        graph.addNode(ru);
        graph.addNode(fr);
        graph.addNode(cz);
        
        graph.getNodes().get(0).addToAdjacencyList(de);
        graph.getNodes().get(1).addToAdjacencyList(ru);
        graph.getNodes().get(2).addToAdjacencyList(fr);
        graph.getNodes().get(3).addToAdjacencyList(cz);
    }
    
    @Test
    public void topologicallyTest() {
        Iterator<Node<Country>> itr = graph.SortTopologically().iterator();
        List<Integer> nodeNumberList = new ArrayList<>();
        while (itr.hasNext()) {
            nodeNumberList.add(graph.getNodes().indexOf(itr.next()));
        }
        
        List<Integer> correctList = new ArrayList<Integer>(){{
            add(0);
            add(1); 
            add(2); 
            add(3); 
            add(4);
        }};
        
        Assert.assertTrue(nodeNumberList.equals(correctList));
    }
    
    @Test
    public void DFSTest() {
        Iterator<Node<Country>> itr = graph.SortTopologically().iterator();
        List<Integer> nodeNumberList = new ArrayList<>();
        while (itr.hasNext()) {
            nodeNumberList.add(graph.getNodes().indexOf(itr.next()));
        }
        
        List<Integer> correctList = new ArrayList<Integer>(){{
            add(0);
            add(1); 
            add(2); 
            add(3); 
            add(4);
        }};
        
        Assert.assertTrue(nodeNumberList.equals(correctList));
    }
    
    @Test
    public void BFSTest() {
        Iterator<Node<Country>> itr = graph.SortTopologically().iterator();
        List<Integer> nodeNumberList = new ArrayList<>();
        while (itr.hasNext()) {
            nodeNumberList.add(graph.getNodes().indexOf(itr.next()));
        }
        
        List<Integer> correctList = new ArrayList<Integer>(){{
            add(0);
            add(1); 
            add(2); 
            add(3); 
            add(4);
        }};
        
        Assert.assertTrue(nodeNumberList.equals(correctList));
    }
    
    @Test
    public void EulerTest() {
        Iterable<Node<Country>> itr = graph.EulerianDirectedPath();
        Assert.assertTrue(itr == null);                
    }
}
