/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.graphs;

import java.awt.geom.Ellipse2D;

/**
 *
 * @author Mikolaj
 */
public class NodeVisualization<T> extends Node<T>{
    private Ellipse2D elipse;    
    
    public NodeVisualization() {}

    public NodeVisualization(Ellipse2D elipse, T object) {
        super(object);
        this.elipse = elipse;
    }

    public Ellipse2D getElipse() {
        return elipse;
    }

    public void setElipse(Ellipse2D elipse) {
        this.elipse = elipse;
    }
}
