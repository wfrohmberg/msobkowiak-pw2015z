/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.graphs.ui;

import com.graphs.Graph;
import com.graphs.Node;
import com.graphs.NodeVisualization;
import com.graphs.exceptions.EulerPathException;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.Timer;

/**
 *
 * @author Mikolaj
 */
public class MainFrame<T> {
    private Logger logger = Logger.getLogger(MainFrame.class.getName());
    private String whatDraw = "";
    private Graph<T> graph;
    private JComponent component;
    private Class<T> clazz;
    private JTextField txtFieldFrom;
    private JTextField txtFieldTo;
    private List<Node<T>> drawPath = new ArrayList<>();
    
    public MainFrame(Class<T> clazz) throws InstantiationException, IllegalAccessException {
        this.clazz = clazz;
        graph = new Graph<>();
        getComponent();
        init();
    }
    
    public void getComponent() {
            component = new JComponent() {
            
            private Shape currentShape = null;
            private NodeVisualization<T> currentNode = null;

            {
                MouseAdapter mouseAdapter = new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {

                        if (whatDraw.equalsIgnoreCase("Edge") || whatDraw.equalsIgnoreCase("Remove") || 
                                whatDraw.equalsIgnoreCase("edit") || whatDraw.equalsIgnoreCase("Move node")) {

                            Iterator<Node<T>> itr = graph.getNodes().iterator();

                            while (itr.hasNext()) {
                                NodeVisualization<T> shape = (NodeVisualization<T>) itr.next();
                                                                
                                if (whatDraw.equalsIgnoreCase("Remove")) {
                                    if (shape.getElipse().contains(e.getPoint())) {
                                        itr.remove();
                                        break;
                                    }
                                    Line2D.Double line = new Line2D.Double(shape.getElipse().getCenterX(), shape.getElipse().getCenterY(), 1, 1);
                                    Iterator<Node<T>> adjIter = shape.getAdjacencyList().iterator();
                                    while (adjIter.hasNext()) {
                                        NodeVisualization<T> adjNode = (NodeVisualization<T>) adjIter.next();
                                        Point2D.Double p = new Point2D.Double(adjNode.getElipse().getCenterX(), adjNode.getElipse().getCenterY());
                                        line.setLine(line.getP1(), p);                                       
                                        
                                        Iterator<Point2D> iterPoint = new LineIterator(line);
                                        while (iterPoint.hasNext()) {                                           
                                            Point2D point2d = iterPoint.next();
                                            if (point2d.equals(e.getPoint())) {
                                                adjIter.remove();
                                            }
                                        }
                                    }
                                    
                                } else if (shape.getElipse().contains(e.getPoint())) {    
                                    if (whatDraw.equalsIgnoreCase("Edge")) {
                                        Ellipse2D.Double elipse = (Ellipse2D.Double) shape.getElipse();
                                        currentShape = new Line2D.Double(elipse.getCenterX(), elipse.getCenterY(), elipse.getCenterX(), elipse.getCenterY());
                                        currentNode = shape;
                                    } else if (whatDraw.equalsIgnoreCase("edit")) {
                                        try {
                                            new EditFrame(shape);
                                        } catch (IllegalArgumentException | IllegalAccessException ex) {
                                            logger.log(Level.SEVERE, null, ex);
                                        }
                                    } else if (whatDraw.equalsIgnoreCase("Move node")) {
                                        currentNode = shape;
                                    }      
                                    break;
                                    
                                }                                
                            }

                        } else if (whatDraw.equalsIgnoreCase("Node")) {
                            currentShape = new Ellipse2D.Double(e.getX()-15, e.getY()-15, 30, 30);
                            try {
                                graph.addNode(new NodeVisualization<>((Ellipse2D) currentShape, clazz.newInstance()));
                            } catch (InstantiationException | IllegalAccessException ex) {
                                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } 
                        
                        repaint();
                    }

                    @Override
                    public void mouseDragged(MouseEvent e) {
                        if (whatDraw.equalsIgnoreCase("Edge") && currentShape != null) {
                            Line2D shape = (Line2D) currentShape;
                            shape.setLine(shape.getP1(), e.getPoint());
                        } else if (whatDraw.equalsIgnoreCase("Move node")) {
                            Ellipse2D.Double ellipse = (Ellipse2D.Double) currentNode.getElipse();
                            ellipse.x = e.getX() - 15;
                            ellipse.y = e.getY() - 15;
                            currentNode.setElipse(ellipse);
                        }
                        repaint();
                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {
                        if (whatDraw.equalsIgnoreCase("edge") && currentShape != null) {
                            Line2D line = (Line2D) currentShape;
                            
                            for (Node<T> node : graph.getNodes()) {   
                                NodeVisualization<T> shape = (NodeVisualization<T>) node;
                                if (shape.getElipse().contains(e.getPoint())) {
                                    Ellipse2D.Double elipse = (Ellipse2D.Double) shape.getElipse();
                                    Point point = (Point) e.getPoint();
                                    point.setLocation(elipse.getCenterX(), elipse.getCenterY());
                                    line.setLine(line.getP1(), point);
                                    currentNode.addToAdjacencyList(shape);
                                }                                
                            }                            
                        }
                        
                        currentShape = null;                        
                        repaint();
                    }
                };
                addMouseListener(mouseAdapter);
                addMouseMotionListener(mouseAdapter);
            }

            @Override
            protected void paintComponent(Graphics g) {
                Graphics2D g2d = (Graphics2D) g;  
                Iterator<Node<T>> itr = graph.getNodes().iterator();
                List<Node<T>> tempNodeList = new ArrayList<>();
                
                if (whatDraw.equalsIgnoreCase("BFS")) {
                    Iterable<Node<T>> iterable = graph.BFS(graph.getNodes().get(Integer.valueOf(txtFieldFrom.getText())), 
                        graph.getNodes().get(Integer.valueOf(txtFieldTo.getText())));
                    iterable.iterator().forEachRemaining(tempNodeList::add);
                    itr = iterable.iterator();
                } else if (whatDraw.equalsIgnoreCase("DFS")) {
                    Iterable<Node<T>> iterable = graph.DFS(graph.getNodes().get(Integer.valueOf(txtFieldFrom.getText())), 
                        graph.getNodes().get(Integer.valueOf(txtFieldTo.getText())));
                    iterable.iterator().forEachRemaining(tempNodeList::add);
                    itr = iterable.iterator();
                } else if (whatDraw.equalsIgnoreCase("Topologically sort") || whatDraw.equalsIgnoreCase("Euler path")) {
                    itr = drawPath.iterator();
                }                                
                                
                while (itr.hasNext()) {
                    Node<T> node = itr.next();
                    NodeVisualization<T> shape = (NodeVisualization<T>) node;
                    g2d.setPaint(Color.BLUE);
                    
                    if (whatDraw.equalsIgnoreCase("Topologically sort") || whatDraw.equalsIgnoreCase("Euler path")) {
                        g2d.setPaint(Color.YELLOW);
                    } 
                    g2d.fill(shape.getElipse());                    
                    g2d.draw(shape.getElipse());
                    g2d.setColor(Color.BLACK);
                    g2d.drawString(String.valueOf(graph.getNodes().indexOf((Node<T>) shape)), (float) shape.getElipse().getCenterX(), (float) shape.getElipse().getCenterY()-20);                         
                    
                    if (!shape.getAdjacencyList().isEmpty() && !whatDraw.equalsIgnoreCase("Topologically sort") && !whatDraw.equalsIgnoreCase("Euler path")) {
                        for (Node<T> adjNode : shape.getAdjacencyList()) {
                            if (((whatDraw.equalsIgnoreCase("BFS") || whatDraw.equalsIgnoreCase("DFS")) && tempNodeList.contains((Node<T>) adjNode)) 
                                    || (!whatDraw.equalsIgnoreCase("BFS") && !whatDraw.equalsIgnoreCase("DFS"))) {
                                g2d.setColor(Color.BLUE);
                                NodeVisualization<T> visual = (NodeVisualization<T>) adjNode;
                                Line2D.Double line2d = new Line2D.Double(visual.getElipse().getCenterX(), visual.getElipse().getCenterY(), shape.getElipse().getCenterX(), shape.getElipse().getCenterY());
                                Point p1 = new Point((int)line2d.getX1(), (int)line2d.getY1()); 
                                Point p2 = new Point((int)line2d.getX2(), (int)line2d.getY2()); 
                                drawArrowHead(g2d, p1, p2, Color.BLACK);

                                if (tempNodeList.contains((Node<T>) adjNode)) {
                                    g2d.setPaint(Color.RED);
                                    g2d.setStroke(new BasicStroke(5));
                                    g2d.draw(line2d);
                                } else {
                                    g2d.setPaint(Color.BLACK);
                                    g2d.setStroke(new BasicStroke(1));
                                    g2d.draw(line2d);
                                }                            
                            }
                        }
                    }
                    if (whatDraw.equalsIgnoreCase("Topologically sort") || whatDraw.equalsIgnoreCase("Euler path")) {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                
                if ("edge".equalsIgnoreCase(whatDraw) && currentShape != null) {
                    g2d.setPaint(Color.BLACK);
                    g2d.draw(currentShape);
                }
            }
        };
    }
    
    public void init() {
        JFrame paint = new JFrame();                
        SpringLayout layout = new SpringLayout();
        JPanel outerPanel = new JPanel(layout);  

        JButton btnEdge = new JButton("Edge");
        btnEdge.addActionListener((ActionEvent e) -> {
            whatDraw = ((JButton) e.getSource()).getText();
        });
        
        JButton btnNode = new JButton("Node");
        btnNode.addActionListener((ActionEvent e) -> {
            whatDraw = ((JButton) e.getSource()).getText();
        });
        
        JButton btnRemove = new JButton("Remove");
        btnRemove.addActionListener((ActionEvent e) -> {
            whatDraw = ((JButton) e.getSource()).getText();
        });
        
        JButton btnEdit = new JButton("Edit");
        btnEdit.addActionListener((ActionEvent e) -> {
            whatDraw = ((JButton) e.getSource()).getText();
        });
        
        JButton btnMoveNode = new JButton("Move node");
        btnMoveNode.addActionListener((ActionEvent e) -> {
            whatDraw = ((JButton) e.getSource()).getText(); 
        });
        
        JButton btnBFS = new JButton("BFS");
        btnBFS.addActionListener((ActionEvent e) -> {
            whatDraw = ((JButton) e.getSource()).getText();  
            component.repaint();
        });
        
        JButton btnDFS = new JButton("DFS");
        btnDFS.addActionListener((ActionEvent e) -> {
            whatDraw = ((JButton) e.getSource()).getText(); 
            component.repaint();
        });                
        
        JLabel topoFinish = new JLabel();
        
        JButton btnTopologically = new JButton("Topologically sort");
        btnTopologically.addActionListener((ActionEvent e) -> {
            whatDraw = ((JButton) e.getSource()).getText();
            Iterator<Node<T>> itr = graph.SortTopologically().iterator();
            drawPath = new ArrayList<>();
            topoFinish.setText("Processing topologically sort");
            Timer timer = drawTimer(itr, topoFinish);
            timer.start();
        });
        
        JButton btnEuler = new JButton("Euler path");
        btnEuler.addActionListener((ActionEvent e) -> {
            whatDraw = ((JButton) e.getSource()).getText();
            Iterable<Node<T>> iterable = graph.EulerianDirectedPath();
            try {
                if (iterable != null) {
                    Iterator<Node<T>> itr = iterable.iterator();
                    drawPath = new ArrayList<>();
                    topoFinish.setText("Processing euler path");
                    Timer timer = drawTimer(itr, topoFinish);
                    timer.start();
                } else {
                    throw new EulerPathException("This is not euler graph!");
                }
            } catch (EulerPathException ex) {
                logger.log(Level.INFO, "Euler path exception: {0}", ex.getMessage());                
            }
        });
        
        JButton btnReset = new JButton("Reset");
        btnReset.addActionListener((ActionEvent e) -> {
            whatDraw = ((JButton) e.getSource()).getText();
            component.repaint();
        });
        
        txtFieldFrom = new JTextField(4);
        txtFieldTo = new JTextField(4);
        JLabel lblFrom = new JLabel("Node number from:");
        JLabel lblTo = new JLabel("Node number to:");
        lblFrom.setLabelFor(txtFieldFrom);
        lblTo.setLabelFor(txtFieldTo);
        
        outerPanel.add(btnEdge);
        layout.putConstraint(SpringLayout.WEST, btnEdge, 10, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, btnEdge, 10, SpringLayout.NORTH, outerPanel);
        outerPanel.add(btnNode);
        layout.putConstraint(SpringLayout.WEST, btnNode, 80, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, btnNode, 10, SpringLayout.NORTH, outerPanel);
        outerPanel.add(btnEdit);
        layout.putConstraint(SpringLayout.WEST, btnEdit, 150, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, btnEdit, 10, SpringLayout.NORTH, outerPanel);
        outerPanel.add(btnRemove);
        layout.putConstraint(SpringLayout.WEST, btnRemove, 210, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, btnRemove, 10, SpringLayout.NORTH, outerPanel);
        outerPanel.add(btnMoveNode);
        layout.putConstraint(SpringLayout.WEST, btnMoveNode, 10, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, btnMoveNode, 50, SpringLayout.NORTH, outerPanel);
        
        outerPanel.add(btnDFS);
        layout.putConstraint(SpringLayout.WEST, btnDFS, 340, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, btnDFS, 10, SpringLayout.NORTH, outerPanel);
        
        outerPanel.add(btnBFS);
        layout.putConstraint(SpringLayout.WEST, btnBFS, 400, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, btnBFS, 10, SpringLayout.NORTH, outerPanel);
        
        outerPanel.add(btnTopologically);
        layout.putConstraint(SpringLayout.WEST, btnTopologically, 460, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, btnTopologically, 10, SpringLayout.NORTH, outerPanel);
                
        outerPanel.add(btnEuler);
        layout.putConstraint(SpringLayout.WEST, btnEuler, 600, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, btnEuler, 10, SpringLayout.NORTH, outerPanel);
        
        outerPanel.add(btnReset);
        layout.putConstraint(SpringLayout.WEST, btnReset, 700, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, btnReset, 10, SpringLayout.NORTH, outerPanel);
        
        outerPanel.add(txtFieldFrom);
        layout.putConstraint(SpringLayout.WEST, txtFieldFrom, 460, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, txtFieldFrom, 50, SpringLayout.NORTH, outerPanel);
        
        outerPanel.add(txtFieldTo);
        layout.putConstraint(SpringLayout.WEST, txtFieldTo, 460, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, txtFieldTo, 70, SpringLayout.NORTH, outerPanel);
        
        outerPanel.add(lblFrom);
        layout.putConstraint(SpringLayout.WEST, lblFrom, 350, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, lblFrom, 50, SpringLayout.NORTH, outerPanel);
        
        outerPanel.add(lblTo);
        layout.putConstraint(SpringLayout.WEST, lblTo, 350, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, lblTo, 70, SpringLayout.NORTH, outerPanel);
                
        outerPanel.add(topoFinish);
        layout.putConstraint(SpringLayout.WEST, topoFinish, 560, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, topoFinish, 50, SpringLayout.NORTH, outerPanel);
        
        outerPanel.add(component); 
        component.setPreferredSize(new Dimension(700, 700));
        component.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        component.setBackground(Color.red);
        layout.putConstraint(SpringLayout.WEST, component, 20, SpringLayout.WEST, outerPanel);
        layout.putConstraint(SpringLayout.NORTH, component, 100, SpringLayout.NORTH, outerPanel);
            
        paint.add(outerPanel);
        paint.setSize(800, 900);
        paint.setLocationRelativeTo(null);
        paint.setVisible(true);
    }
            
    private static void drawArrowHead(Graphics2D g2, Point tip, Point tail, Color color) {
        double phi = Math.toRadians(40);
        double barb = 20;
        g2.setPaint(color);
        g2.setStroke(new BasicStroke(4));
        double dy = tip.y - tail.y;
        double dx = tip.x - tail.x;
        double theta = Math.atan2(dy, dx);
        double x, y, rho = theta + phi;
        for(int j = 0; j < 2; j++) {
            x = tip.x - barb * Math.cos(rho);
            y = tip.y - barb * Math.sin(rho);
            g2.draw(new Line2D.Double(tip.x, tip.y, x, y));
            rho = theta - phi;
        }
    }  
    
    private Timer drawTimer(Iterator<Node<T>> itr, JLabel lbl) {
        Timer timer = new Timer(50, (ActionEvent t) -> {
            if (!itr.hasNext()) {
                ((Timer) t.getSource()).stop();
                lbl.setText("");
            } else {
                drawPath.add(itr.next());
                component.repaint();
            }            
        });

        return timer;
    }
}
