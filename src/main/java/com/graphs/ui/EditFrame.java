/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.graphs.ui;

import com.graphs.Node;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

/**
 *
 * @author Mikolaj
 */
public class EditFrame<T> {
    private JDialog frame;
    private Node<T> node;
    private JPanel panel;
    private JButton btnUpdate;
    
    public EditFrame(Node<T> node) throws IllegalArgumentException, IllegalAccessException {
        this.node = node;
        this.frame = new JDialog();        
        this.panel = new JPanel();
        frame.setSize(400, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        getData();
    }
    
    private void getData() throws IllegalArgumentException, IllegalAccessException {
        SpringLayout layout = new SpringLayout();
        panel.setLayout(layout);
        int w = 10;
        JLabel initLabel = null;
        
        for (String param : (Set<String>) node.getNodeParametrs()) {
            JLabel label = new JLabel(param);
            panel.add(label);
            
            JTextField textField = new JTextField(node.getObjectFieldValue(param), 15);
            label.setLabelFor(textField);
            textField.setName(param);
            panel.add(textField);            
            
            if (initLabel == null) {
                initLabel = label;
            }
            
            layout.putConstraint(SpringLayout.WEST, label, 10, SpringLayout.WEST, panel);
            layout.putConstraint(SpringLayout.NORTH, label, w, SpringLayout.NORTH, panel);

            layout.putConstraint(SpringLayout.WEST, textField, 130, SpringLayout.EAST, initLabel);
            layout.putConstraint(SpringLayout.NORTH, textField, w, SpringLayout.NORTH, panel);
            
            w += 30;
        }
        
        btnUpdate = new JButton("Update");
        btnUpdate.addActionListener((ActionEvent e) -> {
            setData();
            frame.setVisible(false);
        });
        
        panel.add(btnUpdate);
        layout.putConstraint(SpringLayout.WEST, btnUpdate, 10, SpringLayout.WEST, panel);
        layout.putConstraint(SpringLayout.NORTH, btnUpdate, w, SpringLayout.NORTH, panel);
        
        frame.add(panel);
    } 
    
    private void setData() {
        Component[] components = panel.getComponents();
        for (Component component : components) {
            for (String param : (Set<String>) node.getNodeParametrs()) {
                if (param.equalsIgnoreCase(component.getName())) {
                    JTextField textField = (JTextField) component;
                    node.setNodeObjectValue(param, textField.getText());
                }
            }
        }
    }
}
