/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.graphs.ui;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author Mikolaj
 */
public class ChooseJarFrame {
    private JDialog frame;
    private JPanel panel;
    private JButton btnAdd;
    private JButton btnChooser;
    private File selectedFile;
    private JComboBox jComboBox;
    
    public ChooseJarFrame() throws IllegalArgumentException, IllegalAccessException {
        this.frame = new JDialog();        
        this.panel = new JPanel();
        this.jComboBox = new JComboBox();
        frame.setSize(650, 300);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        setActions(); 
        setFrame();               
    }
    
    private void setActions() {
        btnAdd = new JButton("Add class");
        btnAdd.setVisible(false);
        btnAdd.addActionListener((ActionEvent e) -> {
            try {
                Class clazz = Class.forName((String) jComboBox.getSelectedItem());
                new MainFrame(clazz);
                frame.setVisible(false);
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(ChooseJarFrame.class.getName()).log(Level.SEVERE, null, ex);
            }            
        });
        
        btnChooser = new JButton("Choose jar file");
        btnChooser.addActionListener((ActionEvent e) -> {
            JFileChooser fileChooser = new JFileChooser();
            int returnValue = fileChooser.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                try {
                    selectedFile = fileChooser.getSelectedFile();
                    Enumeration<JarEntry> jarFile = new JarFile(selectedFile).entries();
                    while (jarFile.hasMoreElements()) {
                        JarEntry entry = jarFile.nextElement();
                        if (entry.getName().endsWith(".class")) {
                            jComboBox.addItem( entry.getName().replaceAll("/", "\\.").replace(".class", ""));
                        }
                    }
                    ClassPathHack.addFile(selectedFile);
                    if (FilenameUtils.isExtension(selectedFile.toString(), "jar")) {
                        this.btnAdd.setVisible(true);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ChooseJarFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
    }
    
    private void setFrame() {
        SpringLayout layout = new SpringLayout();
        
        panel.setLayout(layout);
        JLabel label = new JLabel("Put class name from jar and press enter");
        panel.add(label);
        panel.add(btnAdd);
        panel.add(btnChooser);
        panel.add(jComboBox);

        layout.putConstraint(SpringLayout.WEST, label, 10, SpringLayout.WEST, panel);
        layout.putConstraint(SpringLayout.NORTH, label, 10, SpringLayout.NORTH, panel);

        layout.putConstraint(SpringLayout.WEST, jComboBox, 50, SpringLayout.EAST, label);
        layout.putConstraint(SpringLayout.NORTH, jComboBox, 10, SpringLayout.NORTH, panel);
        
        layout.putConstraint(SpringLayout.WEST, btnAdd, 10, SpringLayout.WEST, panel);
        layout.putConstraint(SpringLayout.NORTH, btnAdd, 50, SpringLayout.NORTH, panel);
        
        layout.putConstraint(SpringLayout.WEST, btnChooser, 10, SpringLayout.WEST, panel);
        layout.putConstraint(SpringLayout.NORTH, btnChooser, 80, SpringLayout.NORTH, panel);
        
        frame.add(panel);
    }
}
