/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.graphs;

import com.graphs.algorithms.DFS;
import com.graphs.algorithms.Algorithms;
import com.graphs.algorithms.BFS;
import com.graphs.algorithms.EulerianDirectedPath;
import com.graphs.algorithms.TopologicallySort;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Mikolaj
 */
public class Graph<T> implements Algorithms<T>, Iterable<Node<T>>{    
    private List<Node<T>> nodes;    
    
    public Graph() {
        nodes = new ArrayList<>();
    }    
    
    public void addNode(Node<T> node) {
        nodes.add(node);
    }        
    
    public List<Node<T>> getNodes() {
        return nodes;
    }             
    
    public List<Node<T>> removeNode(Node<T> node) {
        nodes.remove(node);
        return nodes;
    }
            
    @Override
    public Iterable<Node<T>> DFS(Node<T> from, Node<T> to) {
        return new DFS<T>(this, from).getPathTo(to);
    }

    @Override
    public Iterable<Node<T>> BFS(Node<T> from, Node<T> to) {
        return new BFS<T>(this, from).getPathTo(to);
    }

    @Override
    public Iterable<Node<T>> SortTopologically() {
        return new TopologicallySort<T>().topologicalSort(this);
    }
    
    @Override
    public Iterable<Node<T>> EulerianDirectedPath() {
        return new EulerianDirectedPath<>(this).cycle();
    }

    @Override
    public Iterator<Node<T>> iterator() {
        return nodes.iterator();
    }
    
}
