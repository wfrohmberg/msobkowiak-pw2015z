/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.graphs.algorithms;

import com.graphs.Graph;
import com.graphs.Node;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Stack;

/**
 *
 * @author Mikolaj
 */
public class EulerianDirectedPath<T> {
    private Stack<Node<T>> cycle = new Stack<>();
    private boolean isEulerian = true;

    public EulerianDirectedPath(Graph<T> graph) {

        Iterator<Integer>[] adj = (Iterator<Integer>[]) new Iterator[graph.getNodes().size()];
        for (int v = 0; v < graph.getNodes().size(); v++) {  
            List<Node<T>> adjNodes = new ArrayList<>(graph.getNodes().get(v).getAdjacencyList());
            List<Integer> table = new ArrayList<>();
            
            for (int i = 0; i < adjNodes.size(); i++) {                
                table.add(graph.getNodes().indexOf(adjNodes.get(i)));
            }

            adj[v] = table.iterator();
        }

        int s = 0;
        for (int v = 0; v < graph.getNodes().size(); v++) {
            if (adj[v].hasNext()) {
                s = v;
                break;
            }
        }

        Stack<Integer> stack = new Stack<>();
        stack.push(s);
        while (!stack.isEmpty()) {
            int v = stack.pop();
            cycle.push(graph.getNodes().get(v));
            int w = v;
            while (adj[w].hasNext()) {
                stack.push(w);
                w = adj[w].next();
            }
            if (w != v) isEulerian = false;
        }

        for (int v = 0; v < graph.getNodes().size(); v++)
            if (adj[v].hasNext()) isEulerian = false;
    }

    public Iterable<Node<T>> cycle() {
        if (!isEulerian) return null;
        return cycle;
    }
}
