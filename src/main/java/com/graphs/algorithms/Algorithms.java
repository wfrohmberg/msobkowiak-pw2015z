/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.graphs.algorithms;

import com.graphs.Node;

/**
 *
 * @author Mikolaj
 */
public interface Algorithms<T> {
    public Iterable<Node<T>> DFS(Node<T> from, Node<T> to);
    public Iterable<Node<T>> BFS(Node<T> from, Node<T> to);
    public Iterable<Node<T>> SortTopologically();
    public Iterable<Node<T>> EulerianDirectedPath();
}
