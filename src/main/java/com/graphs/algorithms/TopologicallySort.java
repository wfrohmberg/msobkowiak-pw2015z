/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.graphs.algorithms;

import com.graphs.Graph;
import com.graphs.Node;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Mikolaj
 */
public class TopologicallySort<T> {

    private void add(Graph graph, boolean[] used, List<Node<T>> res, int u) {
        used[u] = true;
        for (Node node : (Set<Node>) ((Node) graph.getNodes().get(u)).getAdjacencyList()) {
            int v = graph.getNodes().indexOf(node);
            if (!used[v]) {
                add(graph, used, res, v);
            }
        }
        res.add((Node) (graph.getNodes().get(u)));
    }

    public Iterable<Node<T>> topologicalSort(Graph graph) {
        int n = graph.getNodes().size();
        boolean[] used = new boolean[n];
        List<Node<T>> res = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            if (!used[i]) {
                add(graph, used, res, i);
            }
        }
        Collections.reverse(res);
        return res;
    }
}
