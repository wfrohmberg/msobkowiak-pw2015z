/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.graphs.algorithms;

import com.graphs.Graph;
import com.graphs.Node;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

/**
 *
 * @author Mikolaj
 */
public class BFS<T> {
    private int[] edgeTo;
    private boolean[] marked;
    private final int source;
    private Queue<Integer> priorityQueue;
    private List<Node<T>> nodes;

    public BFS(Graph graph, Node<T> source) {
        this.nodes = graph.getNodes();
        this.source = graph.getNodes().indexOf(source);
        edgeTo = new int[graph.getNodes().size()];
        marked = new boolean[graph.getNodes().size()];
        priorityQueue = new PriorityQueue<>(graph.getNodes().size());
        priorityQueue.offer(this.source);
        bfs(graph, this.source);
    }

    public boolean hasPathTo(int vertex) {
        return marked[vertex];
    }

    public Iterable<Node<T>> getPathTo(Node<T> node) {
        int vertex = nodes.indexOf(node);
        Deque<Node<T>> path = new ArrayDeque<>();

        if (!hasPathTo(vertex)) {
            return path;
        }

        for (int w = vertex; w != source; w = edgeTo[w]) {
            path.push(nodes.get(w));
        }

        path.push(nodes.get(source));
        return path;
    }

    private void bfs(Graph graph, int vertex) {
        marked[vertex] = true;
        priorityQueue.offer(vertex);
        
        while (!priorityQueue.isEmpty()) {
            int v = priorityQueue.remove();
            Node<T> node = (Node<T>) graph.getNodes().get(v);
            
            for (Node<T> adjNode : (Set<Node<T>>) node.getAdjacencyList()) {
                int w = graph.getNodes().indexOf(adjNode);
                if (!marked[w]) {
                    edgeTo[w] = v;
                    marked[w] = true;
                    priorityQueue.offer(w);
                }
            }
        }
    }
}
