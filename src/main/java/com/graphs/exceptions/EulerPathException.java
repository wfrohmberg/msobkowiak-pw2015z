/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.graphs.exceptions;

/**
 *
 * @author Mikolaj
 */
public class EulerPathException extends Exception {
    
    public EulerPathException() {
        super();
    }
    
    public EulerPathException(String message) {
        super(message);
    }
}
