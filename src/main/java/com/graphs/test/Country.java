/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.graphs.test;

/**
 *
 * @author Mikolaj
 */
public class Country {
    private String name;
    private String capital;
    private double area;
    private double population;
    
    public Country(String n, String c, double a, double p) {
        this.name = n;
        this.capital = c;
        this.area = a;
        this.population = p;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPopulation() {
        return population;
    }

    public void setPopulation(double population) {
        this.population = population;
    }
    
}
