/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.graphs;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mikolaj
 */
public class Node <T> {
    private Set<Node<T>> adjacencyList;
    private T object;
    private Logger logger = Logger.getLogger(Node.class.getName());

    public Node() {
        adjacencyList = new HashSet<>();        
    }
    
    public Node(T object) {
        this();
        this.object = object;      
    }    
    
    public T getObject() {
        return object;
    }
    
    public Set<Node<T>> getAdjacencyList() {
        return adjacencyList;
    }    
    
    public void addToAdjacencyList(Node<T> to) {
        adjacencyList.add(to);
    }
    
    public void removeFromAdjacencyList(Node<T> node) {
        adjacencyList.remove(node);
    }
    
     public String getObjectFieldValue(String fieldName) throws IllegalArgumentException, IllegalAccessException {          
        Class clazz = object.getClass();
        Class superClazz = clazz.getSuperclass();
        String fieldValue = "";
        
        while (superClazz != null) {
            for (Field f : clazz.getDeclaredFields()) {            
                f.setAccessible(true);
                if (f.getName().equals(fieldName)) {
                    if (f.get(object) != null) {
                        fieldValue = String.valueOf(f.get(object));
                    }
                    break;
                }
            }  
            clazz = superClazz;
            superClazz = clazz.getSuperclass();
        }                
        
        return fieldValue;
    }
     
     public Set<String> getNodeParametrs() { 
        Set<String> paramList = new HashSet<>();        
        
        Class clazz = this.object.getClass();
        Class superClazz = clazz.getSuperclass();

        while (superClazz != null) {
            for (Field f : clazz.getDeclaredFields()) {            
                String param = "get" + f.getName();
                for (Method m : clazz.getDeclaredMethods()) {
                    if (m.getName().equalsIgnoreCase(param)) {
                        paramList.add(f.getName());
                    }                        
                }                
            }  
            clazz = superClazz;
            superClazz = clazz.getSuperclass();
        }
        
        return paramList;
    }

    public void setNodeObjectValue(String param, Object value) {
        try {
            Class clazz = this.getObject().getClass();
            Class superClazz = clazz.getSuperclass();

            Field field = null;
            try {
                field = clazz.getDeclaredField(param);
            } catch (NoSuchFieldException e) {
            }
            if (field == null) {
                clazz = superClazz;
                superClazz = clazz.getSuperclass();

                while (superClazz != null) {
                    try {
                        field = clazz.getDeclaredField(param);
                    } catch (NoSuchFieldException e) {
                    }
                    if (field != null) {
                        break;
                    }
                    clazz = superClazz;
                    superClazz = clazz.getSuperclass();
                }
            }
            if (field != null) {
                field.setAccessible(true);
                Class<?> fieldType = field.getType();

                if (fieldType.equals(Double.class)) {
                    value = Double.valueOf((String) value);
                } else if (fieldType.equals(Integer.class)) {
                    value = Integer.valueOf((String) value);
                } else if (fieldType.equals(Boolean.class)) {
                    value = Boolean.valueOf((String) value);
                } else if (fieldType.equals(BigDecimal.class)) {
                    value =  BigDecimal.valueOf(Double.valueOf((String) value));
                } else if (fieldType.equals(Long.class)) {
                    value = Long.valueOf((String) value);
                }
                field.set(this.getObject(), value);
            } else {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException | IllegalAccessException e) {
            logger.log(Level.INFO, "Cannot set object value", e);
        } catch (ClassCastException ex) {
            logger.log(Level.INFO, "Cannot cast parmeter {0}", param);
        }
    }
}
